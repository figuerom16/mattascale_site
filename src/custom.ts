// You can also use the generator at https://skeleton.dev/docs/generator to create these values for you
import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';
export const custom: CustomThemeConfig = {
	name: 'custom',
	properties: {
		/* =~= Theme Properties =~= */
		"--theme-font-family-base": "system-ui",
		"--theme-font-family-heading": "system-ui",
		"--theme-font-color-base": "0 0 0",
		"--theme-font-color-dark": "255 255 255",
		"--theme-rounded-base": "4px",
		"--theme-rounded-container": "8px",
		"--theme-border-base": "1px",
		/* =~= Theme On-X Colors =~= */
		"--on-primary": "255 255 255",
		"--on-secondary": "255 255 255",
		"--on-tertiary": "255 255 255",
		"--on-success": "255 255 255",
		"--on-warning": "255 255 255",
		"--on-error": "255 255 255",
		"--on-surface": "255 255 255",
		/* =~= Theme Colors  =~= */
		/* primary | #4F46E5 */
		"--color-primary-50": "229 227 251", /* ⬅ #e5e3fb */
		"--color-primary-100": "220 218 250", /* ⬅ #dcdafa */
		"--color-primary-200": "211 209 249", /* ⬅ #d3d1f9 */
		"--color-primary-300": "185 181 245", /* ⬅ #b9b5f5 */
		"--color-primary-400": "132 126 237", /* ⬅ #847eed */
		"--color-primary-500": "79 70 229", /* ⬅ #4F46E5 */
		"--color-primary-600": "71 63 206", /* ⬅ #473fce */
		"--color-primary-700": "59 53 172", /* ⬅ #3b35ac */
		"--color-primary-800": "47 42 137", /* ⬅ #2f2a89 */
		"--color-primary-900": "39 34 112", /* ⬅ #272270 */
		/* secondary | #0d8375 */
		"--color-secondary-50": "219 236 234", /* ⬅ #dbecea */
		"--color-secondary-100": "207 230 227", /* ⬅ #cfe6e3 */
		"--color-secondary-200": "195 224 221", /* ⬅ #c3e0dd */
		"--color-secondary-300": "158 205 200", /* ⬅ #9ecdc8 */
		"--color-secondary-400": "86 168 158", /* ⬅ #56a89e */
		"--color-secondary-500": "13 131 117", /* ⬅ #0d8375 */
		"--color-secondary-600": "12 118 105", /* ⬅ #0c7669 */
		"--color-secondary-700": "10 98 88", /* ⬅ #0a6258 */
		"--color-secondary-800": "8 79 70", /* ⬅ #084f46 */
		"--color-secondary-900": "6 64 57", /* ⬅ #064039 */
		/* tertiary | #1c71d8 */
		"--color-tertiary-50": "221 234 249", /* ⬅ #ddeaf9 */
		"--color-tertiary-100": "210 227 247", /* ⬅ #d2e3f7 */
		"--color-tertiary-200": "198 220 245", /* ⬅ #c6dcf5 */
		"--color-tertiary-300": "164 198 239", /* ⬅ #a4c6ef */
		"--color-tertiary-400": "96 156 228", /* ⬅ #609ce4 */
		"--color-tertiary-500": "28 113 216", /* ⬅ #1c71d8 */
		"--color-tertiary-600": "25 102 194", /* ⬅ #1966c2 */
		"--color-tertiary-700": "21 85 162", /* ⬅ #1555a2 */
		"--color-tertiary-800": "17 68 130", /* ⬅ #114482 */
		"--color-tertiary-900": "14 55 106", /* ⬅ #0e376a */
		/* success | #21811e */
		"--color-success-50": "222 236 221", /* ⬅ #deecdd */
		"--color-success-100": "211 230 210", /* ⬅ #d3e6d2 */
		"--color-success-200": "200 224 199", /* ⬅ #c8e0c7 */
		"--color-success-300": "166 205 165", /* ⬅ #a6cda5 */
		"--color-success-400": "100 167 98", /* ⬅ #64a762 */
		"--color-success-500": "33 129 30", /* ⬅ #21811e */
		"--color-success-600": "30 116 27", /* ⬅ #1e741b */
		"--color-success-700": "25 97 23", /* ⬅ #196117 */
		"--color-success-800": "20 77 18", /* ⬅ #144d12 */
		"--color-success-900": "16 63 15", /* ⬅ #103f0f */
		/* warning | #c64600 */
		"--color-warning-50": "246 227 217", /* ⬅ #f6e3d9 */
		"--color-warning-100": "244 218 204", /* ⬅ #f4dacc */
		"--color-warning-200": "241 209 191", /* ⬅ #f1d1bf */
		"--color-warning-300": "232 181 153", /* ⬅ #e8b599 */
		"--color-warning-400": "215 126 77", /* ⬅ #d77e4d */
		"--color-warning-500": "198 70 0", /* ⬅ #c64600 */
		"--color-warning-600": "178 63 0", /* ⬅ #b23f00 */
		"--color-warning-700": "149 53 0", /* ⬅ #953500 */
		"--color-warning-800": "119 42 0", /* ⬅ #772a00 */
		"--color-warning-900": "97 34 0", /* ⬅ #612200 */
		/* error | #a51d2d */
		"--color-error-50": "242 221 224", /* ⬅ #f2dde0 */
		"--color-error-100": "237 210 213", /* ⬅ #edd2d5 */
		"--color-error-200": "233 199 203", /* ⬅ #e9c7cb */
		"--color-error-300": "219 165 171", /* ⬅ #dba5ab */
		"--color-error-400": "192 97 108", /* ⬅ #c0616c */
		"--color-error-500": "165 29 45", /* ⬅ #a51d2d */
		"--color-error-600": "149 26 41", /* ⬅ #951a29 */
		"--color-error-700": "124 22 34", /* ⬅ #7c1622 */
		"--color-error-800": "99 17 27", /* ⬅ #63111b */
		"--color-error-900": "81 14 22", /* ⬅ #510e16 */
		/* surface | #20273c */
		"--color-surface-50": "222 223 226", /* ⬅ #dedfe2 */
		"--color-surface-100": "210 212 216", /* ⬅ #d2d4d8 */
		"--color-surface-200": "199 201 206", /* ⬅ #c7c9ce */
		"--color-surface-300": "166 169 177", /* ⬅ #a6a9b1 */
		"--color-surface-400": "99 104 119", /* ⬅ #636877 */
		"--color-surface-500": "32 39 60", /* ⬅ #20273c */
		"--color-surface-600": "29 35 54", /* ⬅ #1d2336 */
		"--color-surface-700": "24 29 45", /* ⬅ #181d2d */
		"--color-surface-800": "19 23 36", /* ⬅ #131724 */
		"--color-surface-900": "16 19 29", /* ⬅ #10131d */
	}
}