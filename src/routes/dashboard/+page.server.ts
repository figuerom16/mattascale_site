let favorite:any[] = [
	['Debian', '/dashboard/debian.png', 'https://www.debian.org/', 'Linux OS', false],
	['Cloudflare', '/dashboard/cloudflare.png', 'https://cloudflare.com/', 'Site Service', false],
	['NGINX', '/dashboard/NGINX.png', 'https://nginx.com','Static Webserver',false],
	['Skeleton', '/dashboard/skeleton.png', 'https://www.skeleton.dev/','Compenent Library',false],
	['PostgreSQL','/dashboard/pgsql.png','https://www.postgresql.org/','Relational Database',false],
	['Fiber','/dashboard/fiber.png','https://gofiber.io/','Go Backend Framework',false],
	['SvelteKit','/dashboard/svelte.png','https://kit.svelte.dev/','Typescript Framework',false],
	['LTSP','/dashboard/LTSP.png','https://ltsp.org/','Diskless Linux Clients',false],
	['No Existance','/dashboard/MATTASCALE.png','https://noexistance.mattascale.com','A Dead Site',false]
]
let home:any[] = [
	['Gitlab:Dashboard','/dashboard/MATTASCALE.png','https://gitlab.com/figuerom16/mattascale_site/-/tree/main/src/routes/dashboard'],
	['pfSense','/dashboard/pfsense.png','https://192.168.0.1/'],
	['Wifi','/dashboard/wifi.png','http://192.168.0.2/'],
	['Extender','/dashboard/wifi.png','http://192.168.0.3/'],
	['OpenMediaVault','/dashboard/omv.png','http://192.198.0.4/'],
	['Proxmox','/dashboard/proxmox.png','https://10.0.0.2:8006/']
]

export async function load() {
	let res_array:Promise<Response>[] = []
	for (const fav of favorite){
		res_array.push(fetch(fav[2], {method:'OPTIONS'}))
	}
	const results = await Promise.allSettled(res_array)
	for (const key in favorite){
		if (results[key].status === 'rejected') continue
		//@ts-ignore
		if (results[key].value.status < 500) favorite[key][4] = true
	}
	return {favorite, home}
}