import pandas as pd
import numpy as np
from pyodide.ffi import to_js

def process_file(text):
	df = pd.DataFrame(text[1:], columns=text[0])
	df = df.set_index('ID')
	df = df[df.index != '']
	df['AMOUNT'] = df['AMOUNT'].astype(float)
	df = df[df['TYPE'] != 'CASH']
	df['AMOUNT'] = np.where(df['TYPE'] == 'CREDIT',-1*df['AMOUNT'],df['AMOUNT'])
	df = df.drop('TYPE',axis=1)
	return df

def scrub(files):
	local = process_file(files[0])
	local_sum = local.groupby(['ID']).sum(numeric_only=True).round(2)
	local_sum = local_sum.rename(columns={'AMOUNT':'L_AMOUNT'})
	local = local.join(local_sum,how='inner')

	bank = process_file(files[1])
	bank = bank.rename(columns={'AMOUNT':'B_AMOUNT'})

	detail = local.join(bank,how='outer')
	mismatch = detail[detail['L_AMOUNT'] != detail['B_AMOUNT']]
	local_index = mismatch[mismatch['ACCOUNT'].notna()].index.values
	bank_index = mismatch[mismatch['ACCOUNT'].isna()].index.values
	mismatch = mismatch.drop(['AMOUNT','ACCOUNT'],axis=1).drop_duplicates()
	for l in local_index:
		for b in bank_index:
			if mismatch.at[l,'L_AMOUNT'] == mismatch.at[b,'B_AMOUNT']:
				detail.at[l,'B_AMOUNT'] = detail.at[l,'AMOUNT']
				mismatch = mismatch.drop(index=l)
				detail = detail.drop(index=b)
				break
	if detail['L_AMOUNT'].sum().round(2) != detail['B_AMOUNT'].sum().round(2):
		detail = detail[detail['L_AMOUNT'] != detail['B_AMOUNT']]
		return False,to_js(['ID','ACCOUNT','AMOUNT','L_AMOUNT','B_AMOUNT']),to_js(detail.reset_index().to_numpy().tolist())
	detail = detail.groupby(['ACCOUNT'])['AMOUNT'].sum().round(2)
	return True,to_js(['ACCOUNT','AMOUNT']),to_js(detail.reset_index().to_numpy().tolist())